import { config } from 'dotenv';
config({ path: './.env.testing' });

import { client } from '@/graphql/apollo-client';
import * as dynamoose from 'dynamoose';

jest.setTimeout(30 * 1000);
jest.retryTimes(3);

dynamoose.aws.ddb.local(process.env.MOCK_DYNAMODB_ENDPOINT);

afterAll(() => client.stop());
