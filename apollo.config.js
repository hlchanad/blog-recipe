/**
 * @type {import('apollo').ApolloConfig}
 **/
module.exports = {
  client: {
    service: {
      name: 'blog-api',
      url: 'http://localhost:4000/dev/graphql',
    },
    excludes: ['**/__tests__/**/*', 'src/graphql/schema.graphql'],
  },
};
