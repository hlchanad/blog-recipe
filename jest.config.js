/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'jest-dynalite',
  globals: {
    'ts-jest': {
      tsconfig: '<rootDir>/tsconfig.json',
      isolatedModules: true,
      diagnostics: false,
    },
  },
  moduleFileExtensions: ['js', 'json', 'ts'],
  testRegex: '\\.spec\\.ts$',
  transform: {
    '^.+\\.(j|t)s$': 'ts-jest',
  },
  testPathIgnorePatterns: ['/node_modules/', '/lib/'],
  setupFilesAfterEnv: ['jest-extended', './jest.setup.ts'],
  moduleNameMapper: {
    '^@/graphql/(.*)': '<rootDir>/src/graphql/$1',
    '^@/helpers$': '<rootDir>/src/helpers',
    '^@/test$': '<rootDir>/src/test',
  },
};
