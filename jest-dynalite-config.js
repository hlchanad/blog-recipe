/** @type {import('jest-dynalite/dist/types').Config} */
module.exports = {
  tables: [
    {
      TableName: 'blog-recipe-jest-recipe-post',
      KeySchema: [{ AttributeName: 'recipeId', KeyType: 'HASH' }],
      AttributeDefinitions: [
        { AttributeName: 'recipeId', AttributeType: 'S' },
        { AttributeName: 'postId', AttributeType: 'S' },
      ],
      GlobalSecondaryIndexes: [
        {
          IndexName: 'blog-recipe-jest-recipe-post-gsi-postId',
          KeySchema: [{ AttributeName: 'postId', KeyType: 'HASH' }],
          Projection: { ProjectionType: 'ALL' },
        },
      ],
      BillingMode: 'PAY_PER_REQUEST',
    },
  ],
  basePort: 8000,
};
