import { authToken } from '@/graphql/apollo-client';
import * as UserMutations from '@/graphql/user/mutations';
import * as PostMutations from '@/graphql/post/mutations';
import {
  MutationCreatePostArgs,
  MutationUpdatePostArgs,
  Post,
} from '@/graphql/types';
import { config } from '@/helpers';

async function login(): Promise<string> {
  return UserMutations.login(
    config('blog.username'),
    config('blog.password'),
  ).then(({ token }) => token);
}

async function common<TResult>(fn: () => Promise<TResult>): Promise<TResult> {
  if (!authToken.get()) {
    authToken.set(await login());
  }

  try {
    return await fn();
  } catch (error) {
    if (error.message !== 'not authenticated') {
      console.error('error', error);
      throw error;
    }
  }

  authToken.set(await login());

  return fn();
}

export async function createPost(args: MutationCreatePostArgs): Promise<Post> {
  return common(() => PostMutations.createPost(args));
}

export async function updatePost(args: MutationUpdatePostArgs): Promise<Post> {
  return common(() => PostMutations.updatePost(args));
}
