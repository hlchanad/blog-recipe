import {
  MutationCreatePostArgs,
  MutationUpdatePostArgs,
  Post,
} from '@/graphql/types';
import { config } from '@/helpers';
import * as faker from 'faker';
import { RecipeSeed } from '../../listeners/__tests__/recipe.seed';
import { RecipePostModel } from '../../models';
import * as BlogService from '../blog.service';
import * as RecipeService from '../recipe.service';

let createPostSpy: jest.SpyInstance;
let updatePostSpy: jest.SpyInstance;

beforeEach(() => {
  createPostSpy = jest
    .spyOn(BlogService, 'createPost')
    .mockResolvedValue({ id: faker.random.alphaNumeric(8) } as Post);
  updatePostSpy = jest
    .spyOn(BlogService, 'updatePost')
    .mockImplementation(async ({ id }) => ({ id } as Post));
});

afterEach(() => {
  createPostSpy.mockRestore();
  updatePostSpy.mockRestore();
});

describe('getPostContent', () => {
  it('generates mdx content from recipe', async () => {
    const recipe = RecipeSeed();
    const md = RecipeService.getPostContent(recipe);

    expect(md).toEqual(expect.any(String));
  });
});

describe('upsertRecipePost', () => {
  it('creates if not find in database', async () => {
    const postId = faker.random.alphaNumeric(8);
    const recipe = RecipeSeed();

    createPostSpy.mockResolvedValue({ id: postId } as Post);

    await RecipeService.upsertRecipePost(recipe);

    const recipePost = await RecipePostModel.query('recipeId')
      .eq(recipe._id)
      .exec()
      .then((recipePosts) => (recipePosts.length > 0 ? recipePosts[0] : null));

    expect(recipePost.recipeId).toBe(recipe._id);
    expect(recipePost.postId).toBe(postId);

    expect(createPostSpy).toBeCalled();
    expect(createPostSpy).toBeCalledTimes(1);
    expect(createPostSpy).toBeCalledWith({
      category: config('blog.category'),
      title: recipe.title,
      summary: recipe.desc,
      banner: recipe.thumbnail.imageUrl,
      content: expect.any(String),
      tags: recipe.tags,
    } as MutationCreatePostArgs);
  });

  it('updates if exists in database', async () => {
    const recipe = RecipeSeed();
    const updatedRecipe = RecipeSeed({ _id: recipe._id });

    const recipePost = await RecipePostModel.create({
      recipeId: recipe._id,
      postId: faker.random.alphaNumeric(8),
    });

    await RecipeService.upsertRecipePost(updatedRecipe);

    const updatedRecipePost = await RecipePostModel.query('recipeId')
      .eq(recipePost.recipeId)
      .exec()
      .then((recipePosts) => (recipePosts.length > 0 ? recipePosts[0] : null));

    expect(updatedRecipePost.updatedAt).not.toEqual(recipePost.updatedAt);

    expect(updatePostSpy).toBeCalled();
    expect(updatePostSpy).toBeCalledTimes(1);
    expect(updatePostSpy).toBeCalledWith(
      expect.objectContaining({
        id: recipePost.postId,
        title: updatedRecipe.title,
        summary: updatedRecipe.desc,
        banner: updatedRecipe.thumbnail.imageUrl,
        content: expect.any(String),
        tags: updatedRecipe.tags,
      } as MutationUpdatePostArgs),
    );
  });
});
