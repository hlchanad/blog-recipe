import { authToken } from '@/graphql/apollo-client';
import * as PostMutations from '@/graphql/post/mutations';
import { Post } from '@/graphql/types';
import * as UserMutations from '@/graphql/user/mutations';
import * as faker from 'faker';
import * as BlogService from '../blog.service';
import { BlogSeed } from './blog.seed';

let authTokenSetSpy: jest.SpyInstance;
let createPostSpy: jest.SpyInstance;
let loginSpy: jest.SpyInstance;

beforeEach(() => {
  authTokenSetSpy = jest.spyOn(authToken, 'set');
  createPostSpy = jest
    .spyOn(PostMutations, 'createPost')
    .mockImplementation(async () => {
      if (!authToken.get() || authToken.get() === 'expired token') {
        throw new Error('not authenticated');
      }
      return { id: faker.random.alphaNumeric(8) } as Post;
    });
  loginSpy = jest
    .spyOn(UserMutations, 'login')
    .mockResolvedValue({ token: 'some token' });
});

afterEach(() => {
  authTokenSetSpy.mockRestore();
  loginSpy.mockRestore();
  createPostSpy.mockRestore();
});

describe('common', () => {
  // seems like not able to use rewire,
  // so the following "test" case is not really good

  it('logins and sets authToken first if absent', async () => {
    const post = await BlogService.createPost(BlogSeed());

    expect(post.id).toEqual(expect.any(String));

    expect(loginSpy).toBeCalled();
    expect(loginSpy).toBeCalledTimes(1);

    expect(authTokenSetSpy).toBeCalled();
    expect(authTokenSetSpy).toBeCalledTimes(1);
    expect(authTokenSetSpy).toBeCalledWith('some token');

    expect(createPostSpy).toBeCalled();
    expect(createPostSpy).toBeCalledTimes(1);
  });

  it('sets authToken again if "not authenticated" is returned', async () => {
    authToken.set('expired token');
    const post = await BlogService.createPost(BlogSeed());

    expect(post.id).toEqual(expect.any(String));

    expect(loginSpy).toBeCalled();
    expect(loginSpy).toBeCalledTimes(1);

    expect(authTokenSetSpy).toBeCalled();
    expect(authTokenSetSpy).toBeCalledTimes(2);
    expect(authTokenSetSpy).toBeCalledWith('some token');

    expect(createPostSpy).toBeCalled();
    expect(createPostSpy).toBeCalledTimes(2);
  });

  it('does nothing more if the refreshed token is still invalid', async () => {
    loginSpy.mockResolvedValue({ token: 'expired token' });

    authToken.set('expired token');
    await expect(() => BlogService.createPost(BlogSeed())).rejects.toThrowError(
      'not authenticated',
    );

    expect(loginSpy).toBeCalled();
    expect(loginSpy).toBeCalledTimes(1);

    expect(authTokenSetSpy).toBeCalled();
    expect(authTokenSetSpy).toBeCalledTimes(2);
    expect(authTokenSetSpy).toBeCalledWith('expired token');

    expect(createPostSpy).toBeCalled();
    expect(createPostSpy).toBeCalledTimes(2);
  });
});

describe('createPost', () => {
  it('call gql createPost', async () => {
    authToken.set('some token');
    const post = await BlogService.createPost(BlogSeed());

    expect(post.id).toEqual(expect.any(String));

    expect(createPostSpy).toBeCalled();
  });
});
