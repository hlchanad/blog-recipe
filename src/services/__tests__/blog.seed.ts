import * as faker from 'faker';

export function BlogSeed(args?) {
  return {
    category: faker.random.alphaNumeric(8),
    title: faker.lorem.sentence(),
    content: faker.lorem.paragraphs(2),
    summary: faker.lorem.paragraph(),
    banner: faker.image.imageUrl(),
    tags: Array.from(new Array(faker.datatype.number(4) + 1)).map(() =>
      faker.lorem.word(),
    ),
    ...args,
  };
}
