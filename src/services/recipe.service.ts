import { config } from '@/helpers';
import * as fs from 'fs';
import * as handlebars from 'handlebars';
import * as handlebarsHelpers from 'handlebars-helpers';
import { RecipePostModel } from '../models';
import { Recipe } from '../types';
import * as BlogService from './blog.service';

export function getPostContent(recipe: Recipe): string {
  const hbsTemplate = fs.readFileSync(
    __dirname + '/../templates/content.hbs',
    'utf8',
  );
  handlebarsHelpers({ handlebars });
  return handlebars.compile(hbsTemplate)({ recipe });
}

export async function upsertRecipePost(recipe: Recipe) {
  const recipePost = await RecipePostModel.query('recipeId')
    .eq(recipe._id)
    .exec()
    .then((recipePosts) => (recipePosts.length > 0 ? recipePosts[0] : null));

  if (!recipePost) {
    const post = await BlogService.createPost({
      category: config<string>('blog.category'),
      title: recipe.title,
      summary: recipe.desc,
      banner: recipe.thumbnail.imageUrl,
      content: getPostContent(recipe),
      tags: recipe.tags,
    });

    await RecipePostModel.create({
      recipeId: recipe._id,
      postId: post.id,
    });
  } else {
    await BlogService.updatePost({
      id: recipePost.postId,
      title: recipe.title,
      summary: recipe.desc,
      banner: recipe.thumbnail.imageUrl,
      content: getPostContent(recipe),
      tags: recipe.tags,
    });

    await recipePost.save();
  }
}
