export interface Recipe {
  _id: string;
  title: string;
  desc: string;
  size: string;
  tags?: string[];
  parts: Array<{
    _id: string;
    title: string;
    desc: string;
    ingredients: Array<{
      _id: string;
      name: string;
      quantity: number;
      unit: string;
    }>;
    steps: Array<{
      _id: string;
      line: string;
      autoNextStep: boolean;
      duration: number;
    }>;
  }>;
  images: Array<{
    _id: string;
    imageUrl: string;
  }>;
  thumbnail: {
    _id: string;
    imageUrl: string;
  };
  createdAt: string;
  updatedAt: string;
}

export interface RecipeCreated {
  recipe: Recipe;
}

export interface RecipeUpdated {
  current: Recipe;
  last: Recipe;
}
