export const blog = {
  url: process.env.BLOG_API_URL || 'http://localhost:4000/dev/graphql',
  username: process.env.BLOG_API_USERNAME,
  password: process.env.BLOG_API_PASSWORD,
  category: process.env.BLOG_API_CATEGORY,
};
