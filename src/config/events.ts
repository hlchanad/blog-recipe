export const events = {
  topics: {
    'recipe-created': process.env.SNS_ARN_RECIPE_CREATED,
    'recipe-updated': process.env.SNS_ARN_RECIPE_UPDATED,
  },
};
