import { getTableName } from '@/helpers';
import * as dynamoose from 'dynamoose';
import { Item } from 'dynamoose/dist/Item';

const TABLE_NAME = getTableName('recipe-post');

export class RecipePost extends Item {
  recipeId: string;
  postId: string;
  createdAt: Date;
  updatedAt: Date;
}

const schema = new dynamoose.Schema(
  {
    recipeId: { type: String, required: true },
    postId: {
      type: String,
      required: true,
      index: [
        {
          name: `${TABLE_NAME}-gsi-postId`,
          global: true,
        },
      ],
    },
  },
  {
    saveUnknown: false,
    timestamps: true,
  },
);

export const RecipePostModel = dynamoose.model<RecipePost>(TABLE_NAME, schema, {
  create: false,
});
