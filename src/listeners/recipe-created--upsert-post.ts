import { pubsub } from '@/helpers';
import { RecipeService } from '../services';
import { RecipeCreated } from '../types';

export const handler = async (rawEvent) => {
  const events = pubsub.parseEvent<RecipeCreated>(rawEvent);

  await Promise.all(
    events.map(async ({ recipe }) => RecipeService.upsertRecipePost(recipe)),
  );
};
