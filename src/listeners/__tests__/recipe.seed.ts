import * as faker from 'faker';
import { Recipe } from '../../types';

export function RecipeSeed(args?: Partial<Recipe>): Recipe {
  function randomArray(): any[] {
    return Array.from(new Array(faker.datatype.number(4) + 1));
  }

  return {
    _id: faker.random.alphaNumeric(8),
    title: faker.lorem.sentence(),
    desc: faker.lorem.paragraph(),
    size: faker.lorem.sentence(),
    tags: randomArray().map(() => faker.lorem.word()),
    parts: randomArray().map(() => ({
      _id: faker.random.alphaNumeric(8),
      title: faker.lorem.sentence(),
      desc: faker.lorem.paragraph(),
      ingredients: randomArray().map(() => ({
        _id: faker.random.alphaNumeric(8),
        name: faker.lorem.sentence(),
        quantity: faker.datatype.number(),
        unit: 'g',
      })),
      steps: randomArray().map(() => ({
        _id: faker.random.alphaNumeric(8),
        line: faker.lorem.sentence(),
        autoNextStep: false,
        duration: undefined,
      })),
    })),
    images: randomArray().map(() => ({
      _id: faker.random.alphaNumeric(8),
      imageUrl: faker.image.imageUrl(),
    })),
    thumbnail: {
      _id: faker.random.alphaNumeric(8),
      imageUrl: faker.image.imageUrl(),
    },
    createdAt: new Date().toISOString(),
    updatedAt: new Date().toISOString(),
    ...args,
  };
}
