import { mockEvent } from '@/test';
import { RecipeService } from '../../services';
import { handler } from '../recipe-created--upsert-post';
import { RecipeSeed } from './recipe.seed';

it('calls RecipeService.upsertRecipePost()', async () => {
  const recipe = RecipeSeed();

  const upsertSpy = jest
    .spyOn(RecipeService, 'upsertRecipePost')
    .mockResolvedValue();

  await handler(mockEvent({ recipe }));

  expect(upsertSpy).toBeCalled();
  expect(upsertSpy).toBeCalledTimes(1);
  expect(upsertSpy).toBeCalledWith(recipe);

  upsertSpy.mockRestore();
});
