import { pubsub } from '@/helpers';
import { RecipeService } from '../services';
import { RecipeUpdated } from '../types';

export const handler = async (rawEvent) => {
  const events = pubsub.parseEvent<RecipeUpdated>(rawEvent);

  await Promise.all(
    events.map(async ({ current }) => RecipeService.upsertRecipePost(current)),
  );
};
