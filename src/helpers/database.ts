import { config } from './config';

export function getTableName(table: string): string {
  return `${config('app.name')}-${table}`;
}
