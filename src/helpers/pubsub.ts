export function parseEvent<T>(event): T[] {
  return event.Records.map((record) => {
    const snsEvent = JSON.parse(record.body);
    return JSON.parse(snsEvent.Message) as unknown as T;
  });
}
