import { get, PropertyPath } from 'lodash';
import * as AppConfigs from '../config';

export function config<T>(path: PropertyPath, _default?: T): T {
  return get(AppConfigs, path, _default);
}
