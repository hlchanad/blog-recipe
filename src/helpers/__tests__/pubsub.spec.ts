import { parseEvent } from '../pubsub';
import { mockEvent } from '@/test';

describe('parseEvent()', () => {
  it('parses event', async () => {
    const result = parseEvent(mockEvent({ hello: 'world' }));
    expect(result).toHaveLength(1);
    expect(result[0]).toMatchObject({ hello: 'world' });
  });
});
