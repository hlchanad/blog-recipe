import mockAwsEvents from 'mock-aws-events';

export function mockEvent(payload) {
  const snsEvent = mockAwsEvents('aws:sns', {
    Records: [
      {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        Sns: {
          Message: JSON.stringify(payload),
        },
      },
    ],
  });

  return mockAwsEvents('aws:sqs', {
    Records: [
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      {
        body: JSON.stringify(snsEvent.Records[0].Sns),
      },
    ],
  });
}
