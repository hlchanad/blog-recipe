import { gql } from '@apollo/client/core';
import { client } from '../../apollo-client';
import { MutationCreatePostArgs, Post } from '../../types';

const mutation = gql`
  mutation CreatePost(
    $category: String!
    $title: String!
    $content: String!
    $summary: String
    $banner: String!
    $tags: [String!]
  ) {
    createPost(
      category: $category
      title: $title
      content: $content
      summary: $summary
      banner: $banner
      tags: $tags
    ) {
      id
    }
  }
`;

export async function createPost(post: MutationCreatePostArgs): Promise<Post> {
  const response = await client.mutate<{ createPost: Post }>({
    mutation,
    variables: post,
  });
  return response.data.createPost;
}
