import { gql } from '@apollo/client/core';
import { client } from '../../apollo-client';
import { MutationUpdatePostArgs, Post } from '../../types';

const mutation = gql`
  mutation UpdatePost(
    $id: String!
    $category: String
    $title: String
    $content: String
    $summary: String
    $banner: String
    $tags: [String!]
  ) {
    updatePost(
      id: $id
      category: $category
      title: $title
      content: $content
      summary: $summary
      banner: $banner
      tags: $tags
    ) {
      id
    }
  }
`;

export async function updatePost(post: MutationUpdatePostArgs): Promise<Post> {
  const response = await client.mutate<{ updatePost: Post }>({
    mutation,
    variables: post,
  });
  return response.data.updatePost;
}
