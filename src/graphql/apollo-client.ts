import { ApolloClient, ApolloLink, HttpLink } from '@apollo/client/core';
import { InMemoryCache } from '@apollo/client/cache';
import { config } from '@/helpers';
import fetch from 'cross-fetch';

let _authToken: string;
export const authToken = {
  get: () => _authToken,
  set: (token) => (_authToken = token),
};

export const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: ApolloLink.from([
    // Auth Link
    new ApolloLink((operation, forward) => {
      const auth = authToken.get() ? `Bearer ${authToken.get()}` : undefined;
      operation.setContext(({ headers }) => ({
        headers: {
          ...headers,
          Authorization: auth,
        },
      }));
      return forward(operation);
    }),
    // Normal HTTP Link
    new HttpLink({ uri: config('blog.url'), fetch }),
  ]),
});
