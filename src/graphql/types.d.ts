// THIS FILE IS GENERATED, DO NOT EDIT!
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Date: any;
};

export type Category = Timestamps & {
  __typename?: 'Category';
  createdAt: Scalars['Date'];
  id: Scalars['String'];
  name: Scalars['String'];
  slug: Scalars['String'];
  updatedAt: Scalars['Date'];
};

export type Login = {
  __typename?: 'Login';
  token: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  /** Create category. Required scopes: category:create */
  createCategory?: Maybe<Category>;
  /** Create post. Required scopes: post:create */
  createPost?: Maybe<Post>;
  /** Create user */
  createUser?: Maybe<User>;
  /** User login */
  login?: Maybe<Login>;
  /** Update post. Required scopes: post:update */
  updatePost?: Maybe<Post>;
};

export type MutationCreateCategoryArgs = {
  name: Scalars['String'];
};

export type MutationCreatePostArgs = {
  banner: Scalars['String'];
  category: Scalars['String'];
  content: Scalars['String'];
  summary?: Maybe<Scalars['String']>;
  tags?: Maybe<Array<Scalars['String']>>;
  title: Scalars['String'];
};

export type MutationCreateUserArgs = {
  email: Scalars['String'];
  firstname: Scalars['String'];
  lastname: Scalars['String'];
  password: Scalars['String'];
  username: Scalars['String'];
};

export type MutationLoginArgs = {
  password: Scalars['String'];
  username: Scalars['String'];
};

export type MutationUpdatePostArgs = {
  banner?: Maybe<Scalars['String']>;
  category?: Maybe<Scalars['String']>;
  content?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  summary?: Maybe<Scalars['String']>;
  tags?: Maybe<Array<Scalars['String']>>;
  title?: Maybe<Scalars['String']>;
};

export type PageInfo = {
  __typename?: 'PageInfo';
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
};

export type Post = Timestamps & {
  __typename?: 'Post';
  author: User;
  banner: Scalars['String'];
  category: Category;
  content: Scalars['String'];
  createdAt: Scalars['Date'];
  id: Scalars['String'];
  next?: Maybe<Post>;
  previous?: Maybe<Post>;
  slug: Scalars['String'];
  summary: Scalars['String'];
  tags?: Maybe<Array<Tag>>;
  title: Scalars['String'];
  updatedAt: Scalars['Date'];
};

export type PostPageable = {
  __typename?: 'PostPageable';
  edges: Array<Post>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type PostsFilterInput = {
  author?: Maybe<Scalars['String']>;
  category?: Maybe<Scalars['String']>;
  summary?: Maybe<Scalars['String']>;
  tags?: Maybe<Array<Scalars['String']>>;
  title?: Maybe<Scalars['String']>;
};

export type PostsFilterQueryInput = {
  and?: Maybe<PostsFilterInput>;
  or?: Maybe<PostsFilterInput>;
};

export enum PostsSortEnum {
  LatestCreated = 'LATEST_CREATED',
  LatestUpdated = 'LATEST_UPDATED',
}

export type Query = {
  __typename?: 'Query';
  /** Get post by id or slug */
  post?: Maybe<Post>;
  /** List posts with pagination */
  posts?: Maybe<PostPageable>;
  /** List tags with pagination */
  tags?: Maybe<TagPageable>;
};

export type QueryPostArgs = {
  id?: Maybe<Scalars['String']>;
  slug?: Maybe<Scalars['String']>;
};

export type QueryPostsArgs = {
  filter?: Maybe<PostsFilterQueryInput>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  sort?: Maybe<PostsSortEnum>;
};

export type QueryTagsArgs = {
  filter?: Maybe<TagsFilterInput>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  sort?: Maybe<TagsSortEnum>;
};

export type Tag = Timestamps & {
  __typename?: 'Tag';
  createdAt: Scalars['Date'];
  id: Scalars['String'];
  name: Scalars['String'];
  slug: Scalars['String'];
  stat: TagStat;
  updatedAt: Scalars['Date'];
};

export type TagPageable = {
  __typename?: 'TagPageable';
  edges: Array<Tag>;
  pageInfo?: Maybe<PageInfo>;
  totalCount: Scalars['Int'];
};

export type TagStat = {
  __typename?: 'TagStat';
  count: Scalars['Int'];
};

export type TagsFilterInput = {
  name?: Maybe<Scalars['String']>;
};

export enum TagsSortEnum {
  Popular = 'POPULAR',
}

export type Timestamps = {
  createdAt: Scalars['Date'];
  updatedAt: Scalars['Date'];
};

export type User = Timestamps & {
  __typename?: 'User';
  createdAt: Scalars['Date'];
  email: Scalars['String'];
  firstname: Scalars['String'];
  id: Scalars['String'];
  lastname: Scalars['String'];
  updatedAt: Scalars['Date'];
  username: Scalars['String'];
};
