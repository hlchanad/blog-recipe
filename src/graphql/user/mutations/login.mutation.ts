import { gql } from '@apollo/client/core';
import { client } from '../../apollo-client';
import { Login } from '../../types';

const mutation = gql`
  mutation Login($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      token
    }
  }
`;

export async function login(
  username: string,
  password: string,
): Promise<Login> {
  const response = await client.mutate<{ login: Login }>({
    mutation,
    variables: { username, password },
  });
  return response.data.login;
}
